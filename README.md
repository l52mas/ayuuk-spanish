# Ayuuk-Spanish parallel corpus 

Parallel corpus [Ayuuk/Mixe del Istmo](https://en.wikipedia.org/wiki/Isthmus_Mixe) and Spanish developed at
[IIMAS](https://www.iimas.unam.mx/)-[UNAM](https://www.unam.mx/)


This collection is inspired by the [parallel corpus](https://github.com/pywirrarika/wixarikacorpora/tree/master/parallel-corp) in [Wixarika
corpus](https://github.com/pywirrarika/wixarikacorpora)

For another parallel corpus for the same variant visit:

* https://github.com/DelfinoAyuuk/corpora_ayuuk-spanish_nmt

## ISO 639-3 codes

* Ayuuk/Mixe del Istmo: _mir_ ([Glottolog](https://glottolog.org/resource/languoid/id/lowl1263))
* Spanish: _spa_ ([Glottolog](https://glottolog.org/resource/languoid/id/stan1288a))


# Authors

### Ayuuk/Mixe del Istmo translations

* Martha Elba Ramírez Andrés
* [Lic. Victoriano Santiago Cayetano](https://www.ayuuk.net)
* Lic. Jonathan Santiago Antonio

### Spanish texts

Grimm and Anderssen stories in Public Domain.

### Original selection of phrases

* Manuel Mager

### This corpus selection of phrases

* Delfino Zacarías Marquez

### Project coordinators

* Delfino Zacarías Marquez
* Ivan Vladimir Meza Ruiz

## Files

The corpus files are located in the _files_ folder:

    files/
    ├── corpus.mir          # Ayuuk phrases
    ├── corpus.spa          # Spanish phrases
    ├── full_corpus.csv     # Spanish-Ayuuk phrases 
    └── full_corpus.xlsx    # Full sheets with phrases in both languages

# License

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/l52mas/ayuuk-spanish">Corpus Ayuuk-spanish IIMAS-UNAM</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 

# Acknowledgments

We thank the project “Traducción automática para lenguas indígenas de México”
PAPIIT-IA104420, UNAM. We also thank Unión Nacional de Traductores Indígenas who
help us to identify our excelent translators.
